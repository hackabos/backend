const express = require('express')
const router = express.Router()
const { jwtAuth, validator } = require('../middlewares/index')
const { createValidator, updateValidator, deleteValidator } = require('../models/UserService')
const serviceController = require('../controllers/service.controller')

router.get('/', jwtAuth, serviceController.getAll)
router.get('/me', jwtAuth, serviceController.getUserServices)
router.post('/me', jwtAuth, createValidator(), validator, serviceController.createUserService)
router.patch('/me', jwtAuth, updateValidator(), validator, serviceController.updateUserService)
router.delete('/me', jwtAuth, deleteValidator(), validator, serviceController.deleteUserService)

module.exports = router
