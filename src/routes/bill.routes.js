const express = require('express')
const router = express.Router()
const { jwtAuth, validator } = require('../middlewares/index')
const billController = require('../controllers/bill.controller')
const { refreshBillsValidator, downloadBillsValidator } = require('../models/Bill')

router.get('/', jwtAuth, billController.getAll)
router.post('/refresh', jwtAuth, refreshBillsValidator(), validator, billController.refreshBills)
router.post('/download', jwtAuth, downloadBillsValidator(), validator, billController.downloadBill)

module.exports = router
