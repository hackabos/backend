const express = require('express')
const router = express.Router()
const { jwtAuth, validator } = require('../middlewares/index')
const { signupValidator, loginValidator, profileValidator } = require('../models/User')
const userController = require('../controllers/user.controller')

router.post('/signup', signupValidator(), validator, userController.signup)
router.post('/login', loginValidator(), validator, userController.login)
router.get('/logout', jwtAuth, userController.logout)
router.get('/me', jwtAuth, userController.getProfile)
router.patch('/me', jwtAuth, profileValidator(), validator, userController.updateProfile)

module.exports = router
