const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
require('dotenv').config()
const initializeDB = require('./database/sequelize')
const userRoutes = require('./routes/user.routes')
const serviceRoutes = require('./routes/service.routes')
const billRoutes = require('./routes/bill.routes')
require('./cron.jobs')

const app = express()
const port = process.env.PORT || 8080

// middlewares
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/users', userRoutes)
app.use('/services', serviceRoutes)
app.use('/bills', billRoutes)

initializeDB()
  .then(() => {
    app.listen(port, () => {
      app.emit('ready')
      console.log(`Example app listening on port ${port}!`)
    })
  })
  .catch(err => console.log(err))

// Export app for testing purposes
module.exports = app
