const config = require('config')
const axios = require('axios')
const moment = require('moment')
const { UserService } = require('../models/UserService')

const API = config.get('apple.api')

const scrape = async ({ username, password, token, id: userServiceId }, userTmpDir, scrapeFromDate) => {
  let response

  if (token) {
    response = await _getOrdersData(token, scrapeFromDate)
  }

  if (!token || response.status === 401) {
    const login = await _login(username, password)

    if (login.data && login.data.access_token) {
      await _updateServiceToken(userServiceId, login.data.access_token)
      response = await _getOrdersData(login.data.access_token, scrapeFromDate)
    }
  }

  const ordersData = _formatData(response.data, userServiceId)

  return ordersData
}

const _getOrdersData = async (token, scrapeFromDate) => {
  let response
  let url = `${API}/invoices/get`

  if (scrapeFromDate) {
    url += `?mindate=${moment(scrapeFromDate).valueOf()}`
  }

  try {
    response = await axios({
      method: 'GET',
      url,
      headers: {
        Authorization: `bearer ${token}`
      }
    })
  } catch (error) {
    console.log('GET ORDERS ERROR: ', error.message)
    return error.response
  }

  return response
}

const _login = async (username, password) => {
  let loginResponse

  try {
    loginResponse = await axios({
      method: 'POST',
      url: `${API}/users/setToken`,
      data: {
        email: username,
        password,
        service: 'Apple Store'
      }
    })
  } catch (error) {
    console.log('LOGIN ERROR: ', error.message)
    return error.response
  }

  return loginResponse
}

const _updateServiceToken = async (userServiceId, token) => {
  try {
    await UserService.update({
      token
    },
    {
      where: {
        id: userServiceId
      }
    })
  } catch (error) {
    console.log('UPDATE TOKEN ERROR: ', error.message)
  }
}

const _formatData = (data, userServiceId) => {
  return data.map(order => {
    return {
      number: order.number || null,
      date: order.date || null,
      products: order.products.map(product => product.product),
      amount: order.products.reduce((acc, curr) => acc + curr.price, 0),
      file_url: order.invoice_url,
      userServiceId
    }
  })
}

module.exports = {
  scrape
}
