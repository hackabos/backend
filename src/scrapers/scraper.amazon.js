const webdriver = require('selenium-webdriver')
const fs = require('fs-extra')
require('dotenv').config()
const moment = require('moment')
const config = require('config')
const { initializeWebdriver, LoginFailedException, ElementNotExistsException } = require('./scraper.utils')

const CURRENT_YEAR = new Date().getFullYear()
const BASE_URL = config.get('amazon.login_url')
const LOGIN_URL = `${BASE_URL}&orderFilter=year-${CURRENT_YEAR}`
const INVOICE_LABEL = 'Factura 1'

const scrape = async ({ username, password, userServiceId }, userTmpDir, scrapeFromDate) => {
  const driver = await initializeWebdriver(userTmpDir)
  const ordersData = []
  let yearsLinks = []

  try {
    await fs.remove(userTmpDir)
    await login(driver, username, password)

    yearsLinks = await getYearsLinks(driver)

    if (scrapeFromDate) {
      yearsLinks = yearsLinks.filter(link => linkYearIsSameOrAfter(link, scrapeFromDate))
    }
  } catch (error) {
    await driver.quit()
    return ordersData
  }

  for (const yearLink of yearsLinks) {
    let pageLinks

    try {
      await driver.get(yearLink)
      pageLinks = await getPageLinks(driver)
    } catch (error) {
      continue
    }

    let stopScraper = false
    for (const pageLink of pageLinks) {
      if (stopScraper) break

      let orderLinks = []

      try {
        await driver.get(pageLink)
        orderLinks = await getOrderLinks(driver)
      } catch (error) {
        continue
      }

      for (const orderLink of orderLinks) {
        let orderData = {}

        try {
          await driver.get(orderLink)
          orderData = await getOrderData(driver, userServiceId)
        } catch (error) {
          continue
        }

        if (scrapeFromDate && moment(orderData.date).isBefore(scrapeFromDate)) {
          stopScraper = true
          break
        }

        try {
          await checkHasInvoice(driver)
          await getPdfFromOrder(driver)
          await renamePdfFile(userTmpDir, orderData.number)
        } catch (error) {
          continue
        }

        ordersData.push(orderData)
      }
    }
  }

  await driver.quit()

  return ordersData
}

const linkYearIsSameOrAfter = (link, scrapeFromDate) => {
  const linkYear = link.substr(link.length - 4)
  const dateYear = moment(scrapeFromDate).format('YYYY')

  return linkYear >= dateYear
}

const login = async (driver, username, password) => {
  try {
    await driver.get(LOGIN_URL)
    await driver.findElement(webdriver.By.id('ap_email')).sendKeys(username, webdriver.Key.RETURN)
    await driver.findElement(webdriver.By.id('ap_password')).sendKeys(password, webdriver.Key.RETURN)
    await driver.manage().setTimeouts({ implicit: 10000 })
  } catch (error) {
    throw new LoginFailedException()
  }
}

const getYearsLinks = async (driver) => {
  const yearLinks = []

  try {
    await driver.findElement(webdriver.By.id('a-autoid-1-announce')).click()
    const yearsTabs = await driver.findElements(webdriver.By.xpath('//a[contains(@data-value, "year")]'))
    for (const tab of yearsTabs) {
      const year = await tab.getText()
      yearLinks.push(BASE_URL + year)
    }
  } catch (error) {
    throw new ElementNotExistsException()
  }

  return yearLinks
}

const getPageLinks = async (driver) => {
  const pageLinks = []

  try {
    const pagesList = await driver.findElements(webdriver.By.xpath('//a[contains(@href, "startIndex")]'))
    for (const page of pagesList) {
      const link = await page.getAttribute('href')
      pageLinks.push(link)
    }
  } catch (error) {
    throw new ElementNotExistsException()
  }

  pageLinks.pop() // Delete the last (repeated) page link

  return pageLinks
}

const getOrderLinks = async (driver) => {
  const orderLinks = []

  try {
    const ordersList = await driver.findElements(webdriver.By.xpath('//a[contains(@href, "order-details")]'))

    for (const order of ordersList) {
      const link = await order.getAttribute('href')
      orderLinks.push(link)
    }
  } catch (error) {
    throw new ElementNotExistsException()
  }

  return orderLinks
}

const getOrderData = async (driver, userServiceId) => {
  const AMOUNT_POSITION = 1
  let orderID
  let orderDate
  let orderAmount
  const productsNames = []

  try {
    orderID = await driver.findElement(webdriver.By.xpath('//*[@id="orderDetails"]/div[2]/div[1]/div/span[2]')).getText()
    orderDate = await driver.findElement(webdriver.By.xpath('//*[@id="orderDetails"]/div[2]/div[1]/div/span[1]')).getText()

    const summaryElements = await driver.findElements(webdriver.By.className('a-text-bold'))
    orderAmount = await summaryElements[AMOUNT_POSITION].getText()

    const productsList = await driver.findElements(webdriver.By.xpath('//a[contains(@href, "asin_title")]'))
    for (const product of productsList) {
      const name = await product.getText()
      productsNames.push(name)
    }
  } catch (error) {
    throw new ElementNotExistsException()
  }

  const rawOrderData = {
    id: orderID,
    amount: orderAmount,
    date: orderDate,
    products: productsNames,
    userServiceId
  }

  const formattedData = formatData(rawOrderData)

  return formattedData
}

const formatData = (rawOrderData) => {
  const ID_START_INDEX = 11
  const MONTH_INDEX = 2
  const ORDER_START_INDEX = 12
  const orderID = rawOrderData.id.substring(ID_START_INDEX)
  let orderDate = rawOrderData.date
    .substring(ORDER_START_INDEX)
    .replace(/de/g, '-')
    .split(' ')

  const months = {
    diciembre: 12,
    noviembre: 11,
    octubre: 10,
    septiembre: 9,
    agosto: 8,
    julio: 7,
    junio: 6,
    mayo: 5,
    abril: 4,
    marzo: 3,
    febrero: 2,
    enero: 1
  }

  orderDate[MONTH_INDEX] = months[orderDate[MONTH_INDEX]]

  orderDate = moment(orderDate.join(''), 'DD-MM-YYYY').format('YYYY-MM-DD')

  const amountStartIndex = rawOrderData.amount.lastIndexOf(' ') + 1
  const orderAmount = parseFloat(rawOrderData.amount.substring(amountStartIndex).replace(',', '.'))

  const productsNames = rawOrderData.products

  const formattedData = {
    number: orderID,
    amount: orderAmount,
    date: orderDate,
    products: productsNames,
    userServiceId: rawOrderData.userServiceId
  }

  return formattedData
}

const checkHasInvoice = async (driver) => {
  try {
    await driver.findElement(webdriver.By.xpath('//*[@id="orderDetails"]/div[2]/div[2]/div/span/a/span')).click()
    await driver.findElement(webdriver.By.linkText(INVOICE_LABEL))
  } catch (e) {
    throw new ElementNotExistsException()
  }
}

const getPdfFromOrder = async (driver) => {
  try {
    const dropPopover = await driver.findElement(webdriver.By.xpath('//*[@id="orderDetails"]/div[2]/div[2]/div/span/a/span'))
    dropPopover.click()
    const downloadButton = await driver.findElement(webdriver.By.linkText(INVOICE_LABEL))
    downloadButton.click()
  } catch (e) {
    throw new ElementNotExistsException()
  }
}

const renamePdfFile = async (userTmpDir, orderNumber) => {
  const DEFAULT_INVOICE_NAME = 'Invoice'
  const INVOICE_EXTENSION = '.pdf'

  const defaultFileName = DEFAULT_INVOICE_NAME + INVOICE_EXTENSION
  const newFileName = orderNumber + INVOICE_EXTENSION

  try {
    await new Promise((resolve, reject) => {
      let count = 0
      const i = setInterval(async () => {
        count++
        if (count > 10) {
          clearInterval(i)
          resolve()
        }

        try {
          await fs.rename(`${userTmpDir}/${defaultFileName}`, `${userTmpDir}/${newFileName}`)
          clearInterval(i)
          resolve()
        } catch (e) { }
      }, 1000)
    })
  } catch (error) { }
}

module.exports = {
  scrape
}
