const webdriver = require('selenium-webdriver')
const fs = require('fs-extra')
require('dotenv').config()
const moment = require('moment')
const config = require('config')
const { initializeWebdriver, ElementNotExistsException, FolderNotExistsException } = require('./scraper.utils')

const LOGIN_URL = config.get('r.login_url')
const FIRST_YEAR = 2011

const scrape = async ({ username, password, userServiceId }, userTmpDir, scrapeFromDate) => {
  const driver = await initializeWebdriver(userTmpDir)
  const ordersData = []
  let years = []

  try {
    await fs.remove(userTmpDir)
    await login(driver, username, password)
    await driver.manage().setTimeouts({ implicit: 10000 })

    years = await getYears(driver)

    if (scrapeFromDate) {
      years = years.filter(link => link >= moment(scrapeFromDate).format('YYYY'))
    }
  } catch (error) {
    await driver.quit()
    return ordersData
  }

  let stopScraper = false
  for (const year of years) {
    if (stopScraper || year === FIRST_YEAR) {
      break
    }

    let ordersLinks = []

    try {
      await driver.findElement(webdriver.By.xpath(`//option[@value='${year}']`)).click()
      // Provisional wait to avoid find element that not exists yet
      await new Promise((resolve, reject) => {
        setTimeout(() => { resolve() }, 2000)
      })

      ordersLinks = await getOrderLinks(driver)
    } catch (error) {
      continue
    }

    ordersLinks = ordersLinks.reverse()

    for (const link of ordersLinks) {
      let orderData = []

      try {
        await driver.get(link)
        orderData = await getOrderData(driver, userServiceId)
      } catch (error) {
        continue
      }

      if (scrapeFromDate && moment(orderData.date).isBefore(scrapeFromDate)) {
        stopScraper = true
        break
      }

      const fileName = `${orderData.id}.pdf`

      try {
        await checkIfDownloaded(userTmpDir, fileName)
        await getPdfFromOrder(driver)
      } catch (error) {
        continue
      }

      ordersData.push(orderData)

      try {
        await driver.get(LOGIN_URL)
      } catch (error) {
        continue
      }
    }
  }
  await driver.quit()

  return ordersData
}

const login = async (driver, username, password) => {
  try {
    await driver.get(LOGIN_URL)
    await driver.findElement(webdriver.By.id('dniCif')).sendKeys(username)
    await driver.findElement(webdriver.By.id('contrasena')).sendKeys(password, webdriver.Key.RETURN)
    await driver.manage().setTimeouts({ implicit: 10000 })
    await driver.wait(async () => {
      return driver.findElement(webdriver.By.id('capa_cuentas_facturas'))
    }, 5000)
  } catch (error) {

  }
}

const getYears = async driver => {
  const years = []
  const select = await driver.findElement(webdriver.By.id('combo_anos_facturacion'))
  const options = await select.findElements(webdriver.By.tagName('option'))
  for (const option of options) {
    const year = await option.getAttribute('value')
    years.push(year)
  }
  return years
}

const getOrderLinks = async driver => {
  const ordersLinks = []

  try {
    const ordersList = await driver.findElements(webdriver.By.linkText('detalles'))

    for (const order of ordersList) {
      const link = await order.getAttribute('href')
      ordersLinks.push(link)
    }
  } catch (error) {
    throw new ElementNotExistsException()
  }

  return ordersLinks
}

const getOrderData = async (driver, userServiceId) => {
  let orderID
  let orderAmount
  let orderDate

  try {
    orderID = await driver.findElement(webdriver.By.xpath('//*[@id="capa_datos_factura"]/div/div[1]/div[1]/table/tbody/tr[1]/td[2]/strong')).getText()
    orderAmount = await driver.findElement(webdriver.By.xpath('//*[@id="capa_datos_factura"]/div/div[1]/div[1]/table/tbody/tr[2]/td[2]/strong')).getText()
    orderDate = await driver.findElement(webdriver.By.xpath('//*[@id="capa_datos_factura"]/div/div[1]/div[1]/table/tbody/tr[3]/td[2]/strong')).getText()
  } catch (error) {
    throw new ElementNotExistsException()
  }

  return {
    number: orderID,
    amount: parseFloat(orderAmount.replace(',', '.').replace('€', '')),
    date: moment(orderDate, 'DD/MM/YYYY').format('YYYY-MM-DD'),
    products: ['Telefonía e Internet'],
    userServiceId
  }
}

const getPdfFromOrder = async (driver) => {
  try {
    await driver.findElement(webdriver.By.xpath("//*[@id='capa_datos_factura']/div/div[1]/div[3]/div/a")).click()
  } catch (error) {
    throw new ElementNotExistsException()
  }
}
const checkIfDownloaded = async (userTmpDir, fileName) => {
  const filePath = `${userTmpDir}${fileName.toLowerCase()}`

  try {
    await fs.pathExists(filePath)
  } catch (error) {
    throw new FolderNotExistsException()
  }
}

module.exports = {
  scrape
}

// scrape('36119686A', 'princesa22', 2, '/home/max/Documents/BOOTCAMP-HACKABOS/final_project/backend/tmp/2')
