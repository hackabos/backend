require('dotenv').config()
const fs = require('fs-extra')
const path = require('path')
const { Bill } = require('../models/Bill')
const { UserService } = require('../models/UserService')
const { Service } = require('../models/Service')

const DOWNLOAD_DIR = path.resolve('./tmp')
const FILES_DIR = path.resolve('./files')

const MAX_STACK = 2
let stack = []

const automaticScrape = async () => {
  const userServices = await getAllUserServicesToScrap()

  for (let userService of userServices) {
    if (stack.length === MAX_STACK) {
      await Promise.race(stack)
      stack = []
    }

    userService = userService.toJSON()

    const scrapeFromDate = (userService.bills[0] && userService.bills[0].date) || null
    stack.push(scrape(userService, scrapeFromDate))
  }
}

const manualScrape = async (userServiceId, scrapeFromDate = null) => {
  const userService = await getUserServiceToScrap(userServiceId)

  if (!scrapeFromDate) {
    scrapeFromDate = (userService.bills[0] && userService.bills[0].date) || null
  }

  scrape(userService, scrapeFromDate)
}

const scrape = async (userService, scrapeFromDate) => {
  const scraper = require(`./scraper.${userService.service.shortName}`)
  const userTmpDir = `${DOWNLOAD_DIR}/${userService.id}`

  try {
    await fs.ensureDir(DOWNLOAD_DIR)
    await fs.ensureDir(FILES_DIR)
    await fs.remove(userTmpDir)

    const ordersData = await scraper.scrape(userService, userTmpDir, scrapeFromDate)
    const toSave = await setIdToUpdateIfExists(ordersData.reverse(), userService.id)

    await fs.ensureDir(userTmpDir)
    await fs.copy(userTmpDir, `${FILES_DIR}/${userService.id}`)
    await Bill.bulkCreate(toSave, {
      updateOnDuplicate: ['number', 'date', 'amount', 'products', 'file_url']
    })
    await fs.remove(userTmpDir)
  } catch (error) {
    console.log(error)
    console.log('Can\'t scrape this...')
  }

  console.log('Finish!')
}

const getAllUserServicesToScrap = async () => {
  let userServices

  try {
    userServices = await UserService.findAll({
      include: [
        {
          model: Bill,
          limit: 1,
          order: [
            ['date', 'DESC']
          ],
          attributes: ['id', 'date']
        },
        {
          model: Service,
          attributes: ['id', 'shortName']
        }
      ]
    })
  } catch (error) {
    console.log(error)
  }

  return userServices
}

const getUserServiceToScrap = async userServiceId => {
  let userService

  try {
    userService = await UserService.findOne({
      where: { id: userServiceId },
      include: [
        {
          model: Bill,
          limit: 1,
          order: [
            ['date', 'DESC']
          ],
          attributes: ['id', 'date']
        },
        {
          model: Service,
          attributes: ['id', 'shortName']
        }
      ]
    })

    userService = userService.toJSON()
  } catch (error) {
    console.log(error)
  }

  return userService
}

const setIdToUpdateIfExists = async (data, userServiceId) => {
  const numbers = data.map(row => row.number)
  let userServiceBills = []

  try {
    userServiceBills = await Bill.findAll({
      where: {
        userServiceId,
        number: numbers
      },
      attributes: ['id', 'number']
    })
  } catch (error) {}

  const fetchedNumbers = userServiceBills.map(row => row.number)
  const toReturn = data.map(row => {
    const index = fetchedNumbers.indexOf(row.number)

    if (index === -1) {
      return row
    }

    return { ...row, id: userServiceBills[index].id }
  })

  return toReturn
}

module.exports = {
  automaticScrape,
  manualScrape
}
