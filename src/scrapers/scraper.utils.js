const moment = require('moment')
const webdriver = require('selenium-webdriver')

class InvalidDateException extends Error {}
class FolderNotExistsException extends Error {}
class LoginFailedException extends Error {}
class ElementNotExistsException extends Error {}

const spanishStringDateToTimestamp = date => {
  const regex = /^(?<day>\d+)\s\w+\s(?<month>\w+)\s\w+\s(?<year>\d+)/g
  const result = regex.exec(date)

  if (result === null || !('groups' in result)) {
    throw new InvalidDateException()
  }

  const { day, month, year } = result.groups

  return moment.utc(`${year}-${month}-${day}`, 'YYYY-MMM-DD', 'es').unix()
}

const initializeWebdriver = async userTmpDir => {
  let driver
  const chromeCapabilities = webdriver.Capabilities.chrome()
  const chromeOptions = {
    args: [
      'incognito',
      'start-maximized',
      'disable-gpu',
      'disable-dev-shm-usage',
      'no-sandbox',
      'ignore-certificate-errors',
      'allow-running-insecure-content',
      'allow-insecure-localhost'
    ],
    prefs: {
      'download.default_directory': userTmpDir,
      'download.prompt_for_download': false,
      'download.directory_upgrade': true,
      'plugins.always_open_pdf_externally': true
    }
  }
  chromeCapabilities.set('chromeOptions', chromeOptions)

  try {
    driver = await new webdriver.Builder().withCapabilities(chromeCapabilities).build()
  } catch (error) {
    throw new Error('Web driver failed on initialize')
  }

  return driver
}

module.exports = {
  InvalidDateException,
  FolderNotExistsException,
  LoginFailedException,
  ElementNotExistsException,
  spanishStringDateToTimestamp,
  initializeWebdriver
}
