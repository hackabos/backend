const cron = require('node-cron')
const { automaticScrape } = require('./scrapers/scraper')

cron.schedule('0 0 4 * * *', async () => {
  try {
    await automaticScrape()
  } catch (error) { console.log(error) }
})
