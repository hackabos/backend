const moment = require('moment')
const path = require('path')
const { Bill } = require('../models/Bill')
const { UserService } = require('../models/UserService')
const { User } = require('../models/User')
const { Service } = require('../models/Service')
const { manualScrape } = require('../scrapers/scraper')

const getAll = async (req, res) => {
  let bills
  const billsFilter = {}

  const serviceFilter = req.query.service ? { service: req.query.service } : {}
  if (req.query.number) {
    billsFilter.number = req.query.number
  }
  if (req.query.date) {
    billsFilter.date = moment(req.query.date, 'DD-MM-YYYY HH:mm')
  }

  try {
    bills = await Bill.findAll({
      where: billsFilter,
      attributes: ['number', 'date', 'amount', 'products', 'file_url'],
      include: [
        {
          model: UserService,
          attributes: ['id'],
          required: true,
          include: [
            {
              model: User,
              where: { id: req.user.userId },
              attributes: [],
              required: true
            },
            {
              model: Service,
              attributes: ['service'],
              where: serviceFilter,
              required: true
            }
          ]
        }
      ]
    })
  } catch (error) {
    res.status(500).json(error)
    return
  }

  bills = bills.map(bill => {
    const data = bill.toJSON()
    const { user_service: userService, ...toRet } = data
    toRet.service = { ...userService.service, userServiceId: userService.id }

    return toRet
  })

  res.json(bills)
}

const refreshBills = (req, res) => {
  const body = req.body

  manualScrape(body.userServiceId, body.from || null)

  res.json()
}

const downloadBill = (req, res) => {
  const { userServiceId, number } = req.body
  const filesPath = path.join(__dirname, '../../files')

  res.sendFile(`${filesPath}/${userServiceId}/${number.toLowerCase()}.pdf`, (err) => {
    if (err) {
      res.status(err.status).json(err.message)
    }
  })
}

module.exports = {
  getAll,
  refreshBills,
  downloadBill
}
