const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const config = require('config')
const { User } = require('../models/User')

const SALT_ROUNDS = config.get('jwt.saltRounds')
const EXPIRATION_MINUTES = config.get('jwt.expirationMinutes')

const signup = async (req, res) => {
  let user

  try {
    const password = await bcrypt.hash(req.body.password, SALT_ROUNDS)

    user = await User.create({
      username: req.body.username,
      password: password
    })
  } catch (error) {
    res.status(500).json(error)
  }

  res.json(user)
}

const login = async (req, res) => {
  let user
  let passMatches

  try {
    user = await User.findOne({
      where: {
        username: req.body.username
      },
      attributes: ['id', 'username', 'name', 'surname', 'password', 'role']
    })
  } catch (error) {
    res.status(500).json()
    return
  }

  if (user === null) {
    res.status(401).json({ errors: [{ login: 'Email or password incorrect' }] })
    return
  }

  try {
    passMatches = await bcrypt.compare(req.body.password, user.password)
  } catch (error) {
    res.status(500).json()
    return
  }

  if (!passMatches) {
    res.status(401).json({ errors: [{ login: 'Email or password incorrect' }] })
    return
  }

  const JWT = jwt.sign({
    exp: Math.floor(Date.now() / 1000) + 60 * EXPIRATION_MINUTES,
    userId: user.id,
    username: user.username,
    role: user.role
  }, process.env.JWT_SECRET)

  res.json({
    token: JWT,
    profile: {
      username: user.username,
      name: user.name,
      surname: user.surname
    }
  })
}

const logout = (req, res) => {
  res.json('logout page')
}

const getProfile = async (req, res) => {
  let user

  try {
    user = await User.findOne({
      where: {
        id: req.user.userId
      },
      attributes: ['username', 'name', 'surname']
    })
  } catch (error) {
    res.status(400).json()
    return
  }

  if (user === null) {
    res.status(404).json()
    return
  }

  res.json(user)
}

const updateProfile = async (req, res) => {
  const NUMBER_OF_UPDATES_INDEX = 0
  const UPDATED_ROWS_INDEX = 1
  const FIRST_ROW = 0
  const userId = req.user.userId
  let updatedRows

  try {
    const body = await _sanitizeBody(req.body)

    updatedRows = await User.update(
      { ...body },
      {
        where: { id: userId },
        returning: true
      }
    )
  } catch (error) {
    res.status(400).json()
    return
  }

  if (updatedRows[NUMBER_OF_UPDATES_INDEX] === 0) {
    res.status(404).json()
    return
  }

  const {
    id,
    password,
    active,
    role,
    createdAt,
    updatedAt,
    ...toReturn
  } = updatedRows[UPDATED_ROWS_INDEX][FIRST_ROW].dataValues

  res.json(toReturn)
}

const _sanitizeBody = async body => {
  const sanitizedBody = {}
  const allowedFields = ['username', 'name', 'surname']

  if (body.password) {
    try {
      sanitizedBody.password = await bcrypt.hash(body.password, SALT_ROUNDS)
    } catch (error) {
      throw new Error()
    }
  }

  for (const field of allowedFields) {
    sanitizedBody[field] = body[field]
  }

  return sanitizedBody
}

module.exports = {
  signup,
  login,
  logout,
  getProfile,
  updateProfile
}
