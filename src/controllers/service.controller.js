const { Service } = require('../models/Service')
const { UserService } = require('../models/UserService')
const { manualScrape } = require('../scrapers/scraper')

const getAll = async (req, res) => {
  let services

  try {
    services = await Service.findAll({
      attributes: ['id', 'service']
    })
  } catch (error) {
    res.status(500).json()
    return
  }

  res.json(services)
}

const getUserServices = async (req, res) => {
  let userServices

  try {
    userServices = await UserService.findAll({
      where: {
        userId: req.user.userId
      },
      include: [
        {
          model: Service,
          attributes: ['id', 'service']
        }
      ],
      attributes: ['id', 'username', 'password']
    })
  } catch (error) {
    res.status(500).json()
    return
  }

  res.json(userServices)
}

const createUserService = async (req, res) => {
  const { serviceId, username, password } = req.body
  const userId = req.user.userId
  let findedService

  try {
    await UserService.create({
      serviceId,
      userId,
      username,
      password
    })

    findedService = await _findUserService({ userId, serviceId, username })
  } catch (error) {
    res.status(500).json()
    return
  }

  manualScrape(findedService.id)
  res.json(findedService)
}

const updateUserService = async (req, res) => {
  let findedService
  const { username, password, id } = req.body
  const fieldsToUpdate = {}

  if (username) {
    fieldsToUpdate.username = username
  }

  if (password) {
    fieldsToUpdate.password = password
  }

  try {
    await UserService.update(
      { ...fieldsToUpdate },
      {
        where: {
          id: id,
          userId: req.user.userId
        }
      }
    )
  } catch (error) {
    res.status(500).json()
    return
  }

  try {
    findedService = await _findUserService({ id })
  } catch (error) {
    res.status(500).json()
    return
  }

  res.json(findedService)
}

const deleteUserService = async (req, res) => {
  let deletedRows

  try {
    deletedRows = await UserService.destroy({
      where: {
        id: req.body.id,
        userId: req.user.userId
      },
      returning: true
    })
  } catch (error) {
    res.status(500).json()
    return
  }

  if (deletedRows === 0) {
    res.status(400).json({ errors: [{ service: 'You can\'t delete this service' }] })
    return
  }

  res.json()
}

const _findUserService = async (where) => {
  try {
    return UserService.findOne({
      where,
      attributes: [
        'id',
        'username',
        'password'
      ],
      include: [
        {
          model: Service,
          attributes: ['id', 'service']
        }
      ]
    })
  } catch (error) {}
}

module.exports = {
  getAll,
  getUserServices,
  createUserService,
  updateUserService,
  deleteUserService
}
