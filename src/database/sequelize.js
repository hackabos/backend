const connection = require('./connection')
const { User } = require('../models/User')
const { UserService } = require('../models/UserService')
const { Service } = require('../models/Service')
const { Bill } = require('../models/Bill')

// Model relationships
Service.hasMany(UserService)
UserService.belongsTo(Service)
UserService.belongsTo(User)
UserService.hasMany(Bill)
User.hasMany(UserService)
Bill.belongsTo(UserService)

const intializeDB = async () => {
  try {
    await connection.createSchema('bill_box', { ifNotExists: true })
    await connection.sync({ force: false })
  } catch (error) {
    throw new Error('Database not created properly!')
  }

  console.log('Database & tables created!')
}

module.exports = intializeDB
