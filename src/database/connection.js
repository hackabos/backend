const Sequelize = require('sequelize')

const DATABASE_URL = process.env.NODE_ENV === 'test' ? process.env.DATABASE_URL_TEST : process.env.DATABASE_URL

module.exports = new Sequelize(DATABASE_URL, {
  host: 'postgres',
  dialect: 'postgres',
  databaseVersion: '9.6.6',
  logging: false,
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
})
