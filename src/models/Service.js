const Sequelize = require('sequelize')
const connection = require('../database/connection')

const serviceSchema = (sequelize, type) => {
  return sequelize.define('services', {
    service: {
      type: type.STRING(50),
      allowNull: false,
      trim: true
    },
    shortName: {
      type: type.STRING(50),
      allowNull: false,
      trim: true
    }
  }, { schema: 'bill_box' })
}

// Define model
const Service = serviceSchema(connection, Sequelize)

module.exports = {
  Service
}
