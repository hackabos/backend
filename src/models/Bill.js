const Sequelize = require('sequelize')
const connection = require('../database/connection')
const { body } = require('express-validator')
const moment = require('moment')
const { UserService } = require('./UserService')

const billSchema = (sequelize, type) => {
  return sequelize.define('bills', {
    number: {
      type: type.STRING(100),
      allowNull: false
    },
    date: {
      type: type.DATE,
      allowNull: false
    },
    amount: {
      type: type.DECIMAL,
      allowNull: false
    },
    products: {
      type: type.ARRAY(type.STRING()),
      defaultValue: null
    },
    file_url: {
      type: type.STRING(255),
      allowNull: false
    }
  }, { schema: 'bill_box' })
}

// Define model
const Bill = billSchema(connection, Sequelize)

// Validators
const refreshBillsValidator = () => {
  return [
    body('userServiceId')
      .exists()
      .notEmpty()
      .trim()
      .custom(async (value, { req }) => {
        const userService = await UserService.findOne({
          where: { id: value }
        })

        if (!userService) {
          throw new Error('Unknow service')
        }
        if (userService.userId !== req.user.userId) {
          throw new Error('You don\' have permissions to do that')
        }
      }),
    body('from')
      .optional()
      .trim()
      .custom(async value => {
        if (!moment(value, 'YYYY-MM-DD', true).isValid()) {
          throw new Error('Invalid date format')
        }
      })
  ]
}
const downloadBillsValidator = () => {
  return [
    body('userServiceId')
      .exists()
      .notEmpty()
      .trim()
      .custom(async (value, { req }) => {
        const userService = await UserService.findOne({
          where: { id: value }
        })

        if (!userService) {
          throw new Error('Unknow service')
        }
        if (userService.userId !== req.user.userId) {
          throw new Error('You don\' have permissions to do that')
        }
      }),
    body('number')
      .exists()
      .notEmpty()
      .trim()
  ]
}

module.exports = {
  Bill,
  refreshBillsValidator,
  downloadBillsValidator
}
