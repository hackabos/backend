const Sequelize = require('sequelize')
const connection = require('../database/connection')
const { body, oneOf } = require('express-validator')

const userServiceSchema = (sequelize, type) => {
  return sequelize.define('user_services', {
    username: {
      type: type.STRING(100),
      allowNull: false,
      trim: true
    },
    password: {
      type: type.STRING(50),
      allowNull: false,
      trim: true
    },
    token: {
      type: type.STRING(255),
      defaultValue: null,
      trim: true
    }
  }, { schema: 'bill_box' })
}

// Define model
const UserService = userServiceSchema(connection, Sequelize)

// Validators
const createValidator = () => {
  return [
    body('serviceId')
      .exists()
      .trim()
      .isNumeric(),
    body('username')
      .exists()
      .trim()
      .if((value, { req }) => req.body.serviceId)
      .custom((value, { req }) => _checkIfServiceUsernameExistsByServiceIdAndUsername({
        serviceId: req.body.serviceId,
        username: req.body.username
      })),
    body('password')
      .exists()
      .trim()
  ]
}
const updateValidator = () => {
  return [
    oneOf([
      body('username').exists().notEmpty(),
      body('password').exists().notEmpty()
    ], 'You must provide username or password fields to update service'),
    body('id')
      .exists()
      .trim()
      .isNumeric()
      .custom((value, { req }) => _checkIfUserServiceExistsAndPermissionsById(req.user.userId, req.body.id)),
    body('username')
      .optional()
      .exists()
      .trim()
      .if((value, { req }) => req.body.id)
      .custom((value, { req }) => _checkIfServiceUsernameExistsById(req.body.id, req.body.username)),
    body('password')
      .optional()
      .exists()
      .trim()
  ]
}
const deleteValidator = () => {
  return [
    body('id')
      .exists()
      .trim()
      .isNumeric()
      .custom((value, { req }) => _checkIfUserServiceExistsAndPermissionsById(req.user.userId, req.body.id))
  ]
}

const _checkIfUserServiceExistsAndPermissionsById = async (userId, id) => {
  let userService

  try {
    userService = await UserService.findOne({
      where: { id }
    })
  } catch (error) {}

  if (userService === null) {
    throw new Error('Service doesn\'t exist!')
  }

  if (userService.userId !== userId) {
    throw new Error('You can\'t do that!')
  }

  return userService
}

const _checkIfServiceUsernameExistsByServiceIdAndUsername = async where => {
  let userService

  try {
    userService = await UserService.findOne({
      where
    })
  } catch (error) {}

  if (userService) {
    throw new Error('Username for this service already exists!')
  }

  return userService
}

const _checkIfServiceUsernameExistsById = async (id, username) => {
  let usernameExistsForService

  try {
    const userService = await _checkIfUserServiceExistsAndPermissionsById(id)

    usernameExistsForService = await _checkIfServiceUsernameExistsByServiceIdAndUsername({
      username,
      serviceId: userService.serviceId,
      id: { $not: id }
    })
  } catch (error) {}

  if (usernameExistsForService) {
    throw new Error('Username for this service already exists!')
  }

  return usernameExistsForService
}

module.exports = {
  UserService,
  createValidator,
  updateValidator,
  deleteValidator
}
