const Sequelize = require('sequelize')
const connection = require('../database/connection')
const { body } = require('express-validator')

const userSchema = (sequelize, type) => {
  return sequelize.define('users', {
    username: {
      type: type.STRING(100),
      allowNull: false,
      unique: true,
      trim: true
    },
    password: {
      type: type.STRING(100),
      allowNull: false
    },
    name: {
      type: type.STRING(50),
      defaultValue: null
    },
    surname: {
      type: type.STRING(50),
      defaultValue: null
    },
    role: {
      type: type.STRING(20),
      defaultValue: 'USER'
    },
    active: {
      type: type.BOOLEAN,
      defaultValue: false
    }
  }, { schema: 'bill_box' })
}

// Define model
const User = userSchema(connection, Sequelize)

// Validators
const signupValidator = () => {
  return [
    body('username')
      .trim()
      .normalizeEmail()
      .isEmail()
      .custom(async value => {
        const user = await User.findOne({
          where: {
            username: value
          }
        })

        if (user !== null) {
          throw new Error('Username already exists!')
        }

        return user
      }),
    body('password')
      .trim()
      .isLength({ min: 5 }),
    body('re-password')
      .trim()
      .custom((value, { req }) => {
        if (value !== req.body.password) {
          throw new Error('Passwords don\'t match')
        }

        return value
      })
  ]
}
const loginValidator = () => {
  return [
    body('username')
      .exists()
      .trim()
      .normalizeEmail(),
    body('password')
      .exists()
      .trim()
  ]
}
const profileValidator = () => {
  return [
    body('password')
      .optional()
      .trim()
      .isLength({ min: 5 }),
    body('re-password')
      .if(body('password').exists())
      .notEmpty()
      .trim()
      .custom((value, { req }) => {
        if (value !== req.body.password) {
          throw new Error('Passwords don\'t match')
        }

        return value
      }),
    body('name')
      .optional()
      .trim()
      .isLength({ min: 2 }),
    body('surname')
      .optional()
      .trim()
      .isLength({ min: 2 })
  ]
}

module.exports = {
  User,
  signupValidator,
  loginValidator,
  profileValidator
}
