const jwtAuth = require('./jwtAuth')
const validator = require('./validator')

const jwtAuthMiddleware = (req, res, next) => {
  const bearerToken = jwtAuth.tokenExistsInRequest(req)

  if (!bearerToken) {
    res.status(401).json({ errors: [{ token: 'Access denied. No token provided.' }] })
    return
  }

  const token = jwtAuth.sanitizeBearerToken(bearerToken)
  const payload = jwtAuth.verifyToken(token)

  if (!payload) {
    res.status(401).json({ errors: [{ token: 'Invalid token.' }] })
    return
  }

  req.user = jwtAuth.getUserDataFromPayload(payload)
  next()
}

const validatorMiddleware = (req, res, next) => {
  const errors = validator.validateRequest(req)

  if (!errors) {
    return next()
  }

  return res.status(422).json(validator.formatVaidationErrors(errors))
}

module.exports = {
  jwtAuth: jwtAuthMiddleware,
  validator: validatorMiddleware
}
