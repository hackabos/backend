const jwt = require('jsonwebtoken')

const tokenExistsInRequest = req => {
  const token = req.headers['x-access-token'] || req.headers.authorization

  if (!token) {
    return false
  }

  return token
}

const sanitizeBearerToken = token => {
  return token.replace(/bearer/gi, '').trim()
}

const verifyToken = token => {
  try {
    return jwt.verify(token, process.env.JWT_SECRET)
  } catch (ex) {
    return false
  }
}

const getUserDataFromPayload = payload => {
  const user = {}
  user.userId = payload.userId
  user.username = payload.username
  user.role = payload.role

  return user
}

module.exports = {
  tokenExistsInRequest,
  sanitizeBearerToken,
  verifyToken,
  getUserDataFromPayload
}
