const { validationResult } = require('express-validator')

const formatVaidationErrors = errors => {
  const extractedErrors = []
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }))

  return { errors: extractedErrors }
}

const validateRequest = req => {
  const errors = validationResult(req)

  if (errors.isEmpty()) {
    return false
  }

  return errors
}

module.exports = {
  formatVaidationErrors,
  validateRequest
}
