FROM node:12.14.0

RUN apt-get update &&\
apt-get install -yq wget xvfb libappindicator3-1 fonts-liberation libasound2 libnspr4 libnss3 \
libxss1 lsb-release xdg-utils

# Install Chrome
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install

# Cd into /app
WORKDIR /app

# Copy package.json into app folder
COPY package.json /app

# Install dependencies
RUN npm install
COPY . /app

USER node

# Start server on port 8080
EXPOSE 8080
ENV PORT=8080

# Creating Display
ENV DISPLAY :99

# Start script on Xvfb
CMD Xvfb :99 -screen 0 1920x1080x16 & npm start