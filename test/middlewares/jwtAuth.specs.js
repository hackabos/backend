/* eslint-disable no-undef */
const { assert, expect } = require('chai')
const { tokenExistsInRequest, sanitizeBearerToken, verifyToken, getUserDataFromPayload } = require('../../src/middlewares/jwtAuth')

describe.skip('JWT middleware tests', () => {
  it('Token must exists in request', async () => {
    const fakeReqWithoutToken = { headers: {} }
    const fakeReqWithToken = { headers: { authorization: 'bearer 123qwe123' } }

    assert.isFalse(tokenExistsInRequest(fakeReqWithoutToken))
    assert.equal('bearer 123qwe123', tokenExistsInRequest(fakeReqWithToken))
  })

  it('Sanitizer must return token without bearer word and spaces', async () => {
    const fakeToken = '    BEareR     123qwe123     '

    assert.equal('123qwe123', sanitizeBearerToken(fakeToken))
  })

  it('Verifier returns false on invalid token', async () => {
    const fakeToken = '123qwe123'

    assert.isFalse(verifyToken(fakeToken))
  })

  it('Must get user fields only', async () => {
    const payload = {
      first: 1,
      userId: 123,
      username: 'Max',
      middle: 2,
      role: 'USER',
      foo: 'qweqwe',
      bar: 123123
    }

    const compare = {
      userId: 123,
      username: 'Max',
      role: 'USER'
    }

    expect(compare).to.eql(getUserDataFromPayload(payload))
  })
})
