/* eslint-disable handle-callback-err */
/* eslint-disable no-undef */
const chai = require('chai')
const chaiHttp = require('chai-http')
const jwt = require('jsonwebtoken')
const app = require('../../src/index')
const { User } = require('../../src/models/User')

// Configure chai
chai.use(chaiHttp)
chai.should()

const username = 'user@user.com'
const password = 123456789
const permanentToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjYwMTU3NzcxMDk4OSwidXNlcklkIjoxLCJ1c2VybmFtZSI6InVzZXJAdXNlci5jb20iLCJyb2xlIjoiVVNFUiIsImlhdCI6MTU3NzcxMTA0OX0.93ykB8KEYY-_PTTxFvSrF4wh_Ps9qTccKwycE6JtXJM'
const invalidToken = '123456qweasd'
const expiredToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Nzc0NDIyNTMsInVzZXJJZCI6ImZiZDFhMWVhLWM0NjYtNDk4MC04ZjVmLWNlNTIyODVmYTVmOSIsImlhdCI6MTU3NzQzODY1M30.hpUPQRfzr6Wqj58CeLfQRLkdaWGAYunDZFAeTRs5qw0'

const signupUser = (body = {}) => {
  const {
    username = 'user@user.com',
    password = 123456789,
    rePassword = 123456789
  } = body

  return chai.request(app)
    .post('/users/signup')
    .send({
      username,
      password,
      're-password': rePassword
    })
}

const truncateTableUsers = async () => {
  return User.destroy({
    truncate: true,
    restartIdentity: true,
    cascade: true
  })
}

describe('Users', () => {
  after(done => {
    truncateTableUsers().then(() => done())
  })

  describe('Before create user', () => {
    beforeEach(done => {
      truncateTableUsers().then(() => done())
    })

    it('should return validation errors on signup user', (done) => {
      const body = {
        username: 'user',
        password: 123
      }
      signupUser(body).end((err, res) => {
        const errors = JSON.parse(res.text).errors

        res.should.have.status(422)
        chai.assert.notEqual(undefined, errors[0].username)
        chai.assert.notEqual(undefined, errors[1].password)
        chai.assert.notEqual(undefined, errors[2]['re-password'])
        done()
      })
    })

    it('should create new user', done => {
      signupUser().end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.a('object')
        done()
      })
    })
  })

  describe('After user was created', () => {
    after(done => {
      signupUser().then(() => done())
    })

    describe('Login', () => {
      it('should return validation errors on login user', done => {
        chai.request(app)
          .post('/users/login')
          .send({
            username,
            password
          })
          .end((err, res) => {
            const token = res.text

            try {
              jwt.verify(token, process.env.JWT_SECRET)
              res.should.have.status(200)
            } catch (error) {
              chai.assert.fail()
            }

            done()
          })
      })

      it('should fail if user doesn\'t exists', done => {
        chai.request(app)
          .post('/users/login')
          .send({
            username: 'not.existent.user@mail.com',
            password
          })
          .end((err, res) => {
            res.should.have.status(401)
            done()
          })
      })

      it('should fail if password not match', done => {
        chai.request(app)
          .post('/users/login')
          .send({
            username,
            password: 'what!??'
          })
          .end((err, res) => {
            res.should.have.status(401)
            done()
          })
      })

      it('should return a valid JWT on login', done => {
        chai.request(app)
          .post('/users/login')
          .send({
            username,
            password
          })
          .end((err, res) => {
            const token = res.text

            try {
              jwt.verify(token, process.env.JWT_SECRET)
              res.should.have.status(200)
            } catch (error) {
              chai.assert.fail()
            }

            done()
          })
      })
    })

    describe('Authorizarion with bearer token', done => {
      it('should fail with invalid authorization bearer token', done => {
        chai.request(app)
          .get('/users/logout')
          .set('authorization', invalidToken)
          .end((err, res) => {
            res.should.have.status(401)
            done()
          })
      })

      it('should fail with expired authorization bearer token', done => {
        chai.request(app)
          .get('/users/logout')
          .set('authorization', expiredToken)
          .end((err, res) => {
            res.should.have.status(401)
            done()
          })
      })
    })

    describe('Logout', done => {
      it('should logout a user', done => {
        chai.request(app)
          .get('/users/logout')
          .set('authorization', permanentToken)
          .end((err, res) => {
            res.should.have.status(200)
            done()
          })
      })
    })

    describe('Update profile', done => {
      let token
      before(done => {
        chai.request(app)
          .post('/users/login')
          .send({
            username,
            password
          })
          .end((err, res) => {
            token = res.text
            done()
          })
      })

      it('should update user profile', done => {
        const name = 'My name'
        const surname = 'First'
        chai.request(app)
          .patch('/users/me')
          .set('authorization', token)
          .send({
            name,
            surname,
            foo: 'bar'
          })
          .end((err, res) => {
            res.should.have.status(200)
            chai.assert.strictEqual(name, res.body.name)
            chai.assert.strictEqual(surname, res.body.surname)
            chai.assert.isUndefined(res.body.foo)
            done()
          })
      })
    })
  })
})
