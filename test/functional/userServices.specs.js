/* eslint-disable handle-callback-err */
/* eslint-disable no-undef */
const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../../src/index')
const { User } = require('../../src/models/User')
const { Service } = require('../../src/models/Service')
const { UserService } = require('../../src/models/UserService')

// Configure chai
chai.use(chaiHttp)
chai.should()

/**
 * JWT Payload:
 * {
 *  'userId': 1,
 *  'username': 'user@user.com',
 *  'role': 'USER',
 * }
 */
const permanentToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjYwMTU3NzcxMDk4OSwidXNlcklkIjoxLCJ1c2VybmFtZSI6InVzZXJAdXNlci5jb20iLCJyb2xlIjoiVVNFUiIsImlhdCI6MTU3NzcxMTA0OX0.93ykB8KEYY-_PTTxFvSrF4wh_Ps9qTccKwycE6JtXJM'

const users = [
  {
    username: 'user1@user.com',
    password: '123456789',
    UserService: {

    }
  },
  {
    username: 'user2@user.com',
    password: '123456789'
  }
]

const services = [
  { service: 'Amazon' },
  { service: 'R Telefonía' }
]

const userServices = [
  {
    userId: 1,
    serviceId: 1,
    username: 'service1@user1.com',
    password: 'abcdefg'
  },
  {
    userId: 1,
    serviceId: 2,
    username: 'service2@user1.com',
    password: 'abcdefg'
  },
  {
    userId: 1,
    serviceId: 1,
    username: 'service1_1@user1.com',
    password: 'abcdefg'
  },
  {
    userId: 2,
    serviceId: 1,
    username: 'service1@user2.com',
    password: '11111'
  }
]

const initializeDatabase = async () => {
  await Service.bulkCreate(services)
  await User.bulkCreate(users)
}

const truncateTables = async () => {
  const models = [User, Service, UserService]

  for (let i = 0; i < models.length; i++) {
    models[i].destroy({
      truncate: true,
      restartIdentity: true,
      cascade: true
    })
  }
}

describe('UserServices', () => {
  beforeEach(done => {
    initializeDatabase().then(() => done())
  })

  afterEach(done => {
    truncateTables().then(() => done())
  })

  describe('Get services for logged user', () => {
    it('should return all services for logged user only', done => {
      UserService.bulkCreate(userServices)
        .then(() => {
          chai.request(app)
            .get('/services/me')
            .set('authorization', permanentToken)
            .end((err, res) => {
              res.should.have.status(200)
              chai.assert.equal(3, res.body.length)
              done()
            })
        })
    })
  })

  describe('Create UserService', () => {
    it('should fail validation on create with empty body', done => {
      chai.request(app)
        .post('/services/me')
        .set('authorization', permanentToken)
        .end((err, res) => {
          res.should.have.status(422)
          done()
        })
    })

    it('should fail validation on create with invalid fields', done => {
      chai.request(app)
        .post('/services/me')
        .set('authorization', permanentToken)
        .send({
          serviceId: 'qwe',
          username: 'user@user.com',
          password: '123456789'
        })
        .end((err, res) => {
          const serviceIdError = res.body.errors.filter(error => error.serviceId !== undefined)

          chai.expect(serviceIdError).to.have.lengthOf(1)
          res.should.have.status(422)
          done()
        })
    })

    it('should fail validation on create with username existent for service', done => {
      UserService.bulkCreate(userServices)
        .then(() => {
          chai.request(app)
            .post('/services/me')
            .set('authorization', permanentToken)
            .send({
              serviceId: '1',
              username: 'service1@user1.com',
              password: '123456789'
            })
            .end((err, res) => {
              const usernameExistsError = res.body.errors.filter(error => error.username !== undefined)

              chai.expect(usernameExistsError).to.have.lengthOf(1)
              res.should.have.status(422)
              done()
            })
        })
    })

    it('should create new service for logged user', done => {
      chai.request(app)
        .post('/services/me')
        .set('authorization', permanentToken)
        .send({
          serviceId: '1',
          username: 'user@user.com',
          password: '123456789'
        })
        .end((err, res) => {
          res.should.have.status(200)
          chai.assert.strictEqual('user@user.com', res.body.username)
          chai.assert.strictEqual('Amazon', res.body.service.service)
          done()
        })
    })
  })

  describe('Update UserService', () => {
    beforeEach(done => {
      chai.request(app)
        .post('/services/me')
        .set('authorization', permanentToken)
        .send({
          serviceId: '1',
          username: 'user@user.com',
          password: '123456789'
        })
        .end(() => done())
    })

    it('should fail with not existent id for user service', done => {
      chai.request(app)
        .patch('/services/me')
        .set('authorization', permanentToken)
        .send({
          id: 1111,
          username: 'user@user.com'
        })
        .end((err, res) => {
          res.should.have.status(422)
          done()
        })
    })

    it('should update username for user service', done => {
      chai.request(app)
        .patch('/services/me')
        .set('authorization', permanentToken)
        .send({
          id: 1,
          username: 'user1@user.com'
        })
        .end((err, res) => {
          res.should.have.status(200)
          chai.assert.strictEqual('user1@user.com', res.body.username)
          done()
        })
    })

    it('should update username for user service with same data', done => {
      chai.request(app)
        .patch('/services/me')
        .set('authorization', permanentToken)
        .send({
          id: 1,
          username: 'user@user.com'
        })
        .end((err, res) => {
          res.should.have.status(200)
          chai.assert.strictEqual('user@user.com', res.body.username)
          done()
        })
    })
  })

  describe('Delete UserService', () => {
    beforeEach(done => {
      chai.request(app)
        .post('/services/me')
        .set('authorization', permanentToken)
        .send({
          serviceId: '1',
          username: 'user@user.com',
          password: '123456789'
        })
        .end(() => done())
    })

    it('should delete user service', done => {
      chai.request(app)
        .delete('/services/me')
        .set('authorization', permanentToken)
        .send({
          id: 1
        })
        .end((err, res) => {
          res.should.have.status(200)
          done()
        })
    })
  })
})
