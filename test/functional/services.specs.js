/* eslint-disable handle-callback-err */
/* eslint-disable no-undef */
const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../../src/index')
const { Service } = require('../../src/models/Service')

// Configure chai
chai.use(chaiHttp)
chai.should()

/**
 * JWT Payload:
 * {
 *  "userId": 1,
 *  "username": "user@user.com",
 *  "role": "USER",
 * }
 */
const permanentToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjYwMTU3NzcxMDk4OSwidXNlcklkIjoxLCJ1c2VybmFtZSI6InVzZXJAdXNlci5jb20iLCJyb2xlIjoiVVNFUiIsImlhdCI6MTU3NzcxMTA0OX0.93ykB8KEYY-_PTTxFvSrF4wh_Ps9qTccKwycE6JtXJM'

const data = [
  { service: 'Amazon' },
  { service: 'R Telefonía' }
]

const createTestServices = async () => {
  return Service.bulkCreate(data)
}

const truncateTableServices = async () => {
  return Service.destroy({
    truncate: true,
    restartIdentity: true,
    cascade: true
  })
}

describe('Services', () => {
  before(done => {
    createTestServices()
      .then(() => done())
  })

  after(done => {
    truncateTableServices()
      .then(() => done())
  })

  it('should return all services', (done) => {
    chai.request(app)
      .get('/services')
      .set('authorization', permanentToken)
      .end((err, res) => {
        res.should.have.status(200)
        chai.assert.equal(data.length, res.body.length)
        done()
      })
  })
})
